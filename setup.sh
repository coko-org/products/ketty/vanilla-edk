#!/bin/bash
set -e

[ -e editoria ] || git clone git@gitlab.coko.foundation:editoria/editoria.git
[ -e editoria-vanilla ] || git clone git@gitlab.coko.foundation:editoria/editoria-vanilla.git
[ -e editoria-templates ] || git clone git@gitlab.coko.foundation:editoria/editoria-templates.git
[ -e wax ] || git clone git@gitlab.coko.foundation:wax/wax.git
